﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.Contact.Common
{
    public static class Constants
    {
        public const string crmd_expirydate = "crmd_expirydate";
        public const string crmd_ccexpirydate = "crmd_ccexpirydate";
        public const string crmd_autorenewmembership = "crmd_autorenewmembership";
        public const string crmd_autorenewchangedate = "crmd_autorenewchangedate";
        public const string crmd_impliedexpirydate = "crmd_impliedexpirydate";
        public const string crmd_implied = "crmd_implied";
        public const string crmd_expressdate = "crmd_expressdate";
        public const string donotbulkemail = "donotbulkemail";

        public static DateTime MinCRMDate = new DateTime(1901, 1, 1);
     }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMD.CPSA.Plugin.Contact.Common
{
    public static class Enums
    {
        public enum AutoRenewMembership
        {
            Yes = 790410000,
            No = 790410001
        }
    }
}

﻿using Microsoft.Xrm.Sdk;
using System;

namespace CRMD.CPSA.Plugin.Contact.Common
{
    public abstract class PluginBase
    {
        #region "Propeprties"
        protected IOrganizationService Service { get; private set; }
        protected IPluginExecutionContext Context { get; private set; }
        protected ITracingService Tracing { get; set; }
        protected Entity TargetEntity { get; private set; }
        protected Entity PreEntity { get; private set; }
        protected Entity PostEntity { get; private set; }
        public string PreEntityImageKey { get; set; }
        public string PostEntityImageKey { get; set; }
        #endregion

        public void Initialize(IServiceProvider serviceProvider)
        {
            this.Context = serviceProvider.GetService(typeof(IPluginExecutionContext)) as IPluginExecutionContext;
            this.Tracing = serviceProvider.GetService(typeof(ITracingService)) as ITracingService;

            if (this.Context != null)
            {
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                this.Service = serviceFactory.CreateOrganizationService(this.Context.UserId);

                if (this.Context.InputParameters.Contains("Target") && Context.InputParameters["Target"] is Entity)
                    this.TargetEntity = this.Context.InputParameters["Target"] as Entity;

                if (string.IsNullOrEmpty(this.PreEntityImageKey))
                    this.PreEntityImageKey = "PreEntityImage";
                if (string.IsNullOrEmpty(this.PostEntityImageKey))
                    this.PostEntityImageKey = "PostEntityImage";

                if (this.Context.PreEntityImages.Contains(this.PreEntityImageKey))
                    this.PreEntity = this.Context.PreEntityImages[this.PreEntityImageKey] as Entity;

                if (this.Context.PostEntityImages.Contains(this.PostEntityImageKey))
                    this.PostEntity = this.Context.PostEntityImages[this.PostEntityImageKey] as Entity;
            }
        }
    }
}

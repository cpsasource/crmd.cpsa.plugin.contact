﻿using CRMD.CPSA.Plugin.Contact.Common;
using Microsoft.Xrm.Sdk;
using System;

namespace CRMD.CPSA.Plugin.Contact
{
    /// <summary>
    /// Post op on Contact update, with PostImage
    /// </summary>
    public class ContactUpdate : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            base.PostEntityImageKey = "PostImage";
            base.Initialize(serviceProvider);

            if (this.TargetEntity == null)
                return;
            
            if (this.Context.Depth > 6)
                return;
            
            UpdateCCAndAutoRenew();
        }

        #region "Private Methods"
        private void UpdateCCAndAutoRenew()
        {
            bool willUpdateCCAndAutoRenew = false;
            Entity contactToUpdateCC = new Entity("contact") { Id = this.TargetEntity.Id };

            //Updating CC expiry date with proper date
            if (this.TargetEntity.Attributes.Contains(Constants.crmd_expirydate))
            {
                string strExpiryDate = this.TargetEntity.GetAttributeValue<string>(Constants.crmd_expirydate);
                if (!string.IsNullOrEmpty(strExpiryDate))
                {
                    if (strExpiryDate.Length == 4)
                    {
                        string monthPart = strExpiryDate.Substring(2, 2);
                        string yearPart = "20" + strExpiryDate.Substring(0, 2);
                        string dayPart = DateTime.DaysInMonth(int.Parse(yearPart), int.Parse(monthPart)).ToString();

                        DateTime expiryDate = new DateTime(int.Parse(yearPart), int.Parse(monthPart), int.Parse(dayPart));
                        if (this.PostEntity.GetAttributeValue<DateTime>(Constants.crmd_ccexpirydate).Date != expiryDate.Date)
                        {
                            contactToUpdateCC[Constants.crmd_ccexpirydate] = expiryDate;
                            willUpdateCCAndAutoRenew = true;
                        }
                    }
                }
                else
                {
                    contactToUpdateCC[Constants.crmd_ccexpirydate] = new System.Nullable<DateTime>();
                    willUpdateCCAndAutoRenew = true;
                }
            }
            
            //Update the AutoRenew Date when the AutoRenew option is changed
            if (this.TargetEntity.Attributes.Contains(Constants.crmd_autorenewmembership))
            {
                var autoRenewMembership = this.TargetEntity.GetAttributeValue<OptionSetValue>(Constants.crmd_autorenewmembership);
                if(autoRenewMembership != null && autoRenewMembership.Value == (int)Enums.AutoRenewMembership.Yes)
                {
                    var dtAutoRenewChangeDate = this.PostEntity.GetAttributeValue<DateTime>(Constants.crmd_autorenewchangedate);
                    if (dtAutoRenewChangeDate != null && dtAutoRenewChangeDate.Date != DateTime.Today.Date)
                    {
                        contactToUpdateCC[Constants.crmd_autorenewchangedate] = DateTime.Today;
                        willUpdateCCAndAutoRenew = true;
                    }
                }
            }

            bool targetHasImpliedExpiryDate = this.TargetEntity.Attributes.Contains(Constants.crmd_impliedexpirydate);
            if (targetHasImpliedExpiryDate && this.TargetEntity.GetAttributeValue<DateTime>(Constants.crmd_impliedexpirydate) > Constants.MinCRMDate)
            {
                if (!this.PostEntity.GetAttributeValue<bool>(Constants.crmd_implied) && !this.TargetEntity.Attributes.Contains(Constants.crmd_implied))
                {
                    contactToUpdateCC[Constants.crmd_implied] = true;
                    willUpdateCCAndAutoRenew = true;
                }
            }

            //If altus_expressdate/impliedDate contains date, then "donotbulkemail" should be false, as it default to true
            //If altus_expressdate/impliedDate do not contain date, then set "donotbulkemail" true
            if (this.TargetEntity.Attributes.Contains(Constants.crmd_impliedexpirydate) || targetHasImpliedExpiryDate)
            {
                if (this.TargetEntity.GetAttributeValue<DateTime>(Constants.crmd_expressdate) > Constants.MinCRMDate || this.TargetEntity.GetAttributeValue<DateTime>(Constants.crmd_impliedexpirydate) > Constants.MinCRMDate ||
                    (this.TargetEntity.GetAttributeValue<DateTime>(Constants.crmd_expressdate) < Constants.MinCRMDate && this.PostEntity.GetAttributeValue<DateTime>(Constants.crmd_impliedexpirydate) > Constants.MinCRMDate) || (this.TargetEntity.GetAttributeValue<DateTime>(Constants.crmd_impliedexpirydate) < Constants.MinCRMDate && this.PostEntity.GetAttributeValue<DateTime>(Constants.crmd_expressdate) > Constants.MinCRMDate))
                {
                    if (this.PostEntity.GetAttributeValue<bool>(Constants.donotbulkemail))
                    {
                        contactToUpdateCC[Constants.donotbulkemail] = false;
                        willUpdateCCAndAutoRenew = true;
                    }
                }
                else
                {
                    if (!this.PostEntity.GetAttributeValue<bool>(Constants.donotbulkemail))
                    {
                        contactToUpdateCC[Constants.donotbulkemail] = true;
                        willUpdateCCAndAutoRenew = true;
                    }
                }
            }

            if (willUpdateCCAndAutoRenew)
            {
                this.Service.Update(contactToUpdateCC);
            }
        }
        #endregion
    }
}
